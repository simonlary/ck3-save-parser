module.exports = {
	parser: "@typescript-eslint/parser",
	parserOptions: {
	  ecmaVersion: 2020,
	},
	ignorePatterns: [".eslintrc.js", "webpack.config.js"],
	extends: [
	  "prettier",
	  "prettier/@typescript-eslint",
	  "plugin:@typescript-eslint/recommended",
	  "plugin:prettier/recommended",
	],
}
