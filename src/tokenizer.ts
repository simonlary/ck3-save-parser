const WHITESPACE = [" ", "\t", "\b", "\n", "\r"];
const SPECIAL_CHARACTER = ["{", "}", "="];
const NUMBER = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "-"];

interface TokenizeResult {
	endIndex: number;
	token: string;
}

function tokenizeNumber(text: string, startIndex: number): TokenizeResult {
	for (let i = startIndex; i < text.length; i++) {
		const character = text.charAt(i);
		if (WHITESPACE.includes(character) || SPECIAL_CHARACTER.includes(character)) {
			return {
				endIndex: i,
				token: text.slice(startIndex, i),
			};
		}
	}

	return {
		endIndex: text.length,
		token: text.slice(startIndex, text.length),
	};
}

function tokenizeQuotedString(text: string, startIndex: number): TokenizeResult {
	for (let i = startIndex; i < text.length; i++) {
		const character = text.charAt(i);
		if (character === '"') {
			return {
				endIndex: i + 1,
				token: text.slice(startIndex, i),
			};
		}
	}

	return {
		endIndex: text.length,
		token: text.slice(startIndex, text.length),
	};
}

function tokenizeSimpleString(text: string, startIndex: number): TokenizeResult {
	for (let i = startIndex; i < text.length; i++) {
		const character = text.charAt(i);
		if (WHITESPACE.includes(character) || SPECIAL_CHARACTER.includes(character)) {
			return {
				endIndex: i,
				token: text.slice(startIndex, i),
			};
		}
	}

	return {
		endIndex: text.length,
		token: text.slice(startIndex, text.length),
	};
}

export default function tokenize(text: string): string[] {
	const tokens: string[] = [];

	let i = 0;
	while (i < text.length) {
		const character = text.charAt(i);

		if (WHITESPACE.includes(character)) {
			// Skip whitespaces
			i++;
		} else if (SPECIAL_CHARACTER.includes(character)) {
			// Check if it's a "structural" character of the file
			tokens.push(character);
			i++;
		} else if (NUMBER.includes(character)) {
			// Check if it's a number (number also includes dates in the form : 1398.3.1)
			const { endIndex, token } = tokenizeNumber(text, i);
			tokens.push(token);
			i = endIndex;
		} else if (character === '"') {
			// Check if it's a quoted string
			const { endIndex, token } = tokenizeQuotedString(text, i + 1);
			tokens.push(token);
			i = endIndex;
		} else {
			// Otherwise it's simple string
			const { endIndex, token } = tokenizeSimpleString(text, i);
			tokens.push(token);
			i = endIndex;
		}
	}

	return tokens;
}
