import JSZip from "jszip";
import tokenize from "./tokenizer";
import { parse } from "./parser";
import { Save } from "./save";

type Data = Parameters<typeof JSZip.loadAsync>[0];

export async function parseFile(data: Data): Promise<Save> {
	const zip = await JSZip.loadAsync(data);
	const content = await zip.files.gamestate.async("string");
	return parseContent(content);
}

export function parseContent(content: string): Save {
	const tokens = tokenize(content);
	const save = parse(tokens);
	return save;
}

export { Save } from "./save";
