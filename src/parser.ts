import { Save } from "./save";

type Dictionary = { [key in string]: DictionaryValue }; // Can't use Record<string, DictionaryValue> because it causes a circular reference error
type DictionaryValue = string | Array<DictionaryValue> | Dictionary;

interface ParseResult<T> {
	endIndex: number;
	result: T;
}

function parseDictionaryOrArray(
	tokens: string[],
	startIndex: number
): ParseResult<Dictionary | Array<DictionaryValue>> {
	if (tokens[startIndex + 1] === "=") {
		// This is an object
		return parseDictionary(tokens, startIndex);
	}
	// This is an array
	return parseArray(tokens, startIndex);
}

function parseArray(tokens: string[], startIndex: number): ParseResult<Array<DictionaryValue>> {
	const array: Array<DictionaryValue> = [];

	let i = startIndex;
	while (i < tokens.length) {
		const token = tokens[i];

		// End of the array
		if (token === "}") {
			return {
				endIndex: i + 1,
				result: array,
			};
		}

		// This is another dictionary or array
		if (token === "{") {
			const { endIndex, result } = parseDictionaryOrArray(tokens, i + 1);
			i = endIndex;
			array.push(result);
		} else {
			// Just add the primitive to the array
			array.push(token);
			i++;
		}
	}

	return {
		endIndex: tokens.length,
		result: array,
	};
}

function parseDictionary(tokens: string[], startIndex: number): ParseResult<Dictionary> {
	const dictionary: Dictionary = {};

	let i = startIndex;
	while (i < tokens.length) {
		const key = tokens[i];
		const equal = tokens[i + 1];
		const valueToParse = tokens[i + 2];

		// We've reached the end of the object
		if (key === "}") {
			return {
				endIndex: i + 1,
				result: dictionary,
			};
		}

		// At this points equal should always be an equal sign. If not, this is an invalid file
		if (equal !== "=") {
			throw new Error("Invalid save file. (missing '=' sign)");
		}

		let valueParsed: DictionaryValue;
		switch (valueToParse) {
			case "{": {
				// This is another dictionary or array
				const { endIndex, result } = parseDictionaryOrArray(tokens, i + 3);
				i = endIndex;
				valueParsed = result;
				break;
			}
			case "rgb": {
				// This is an RGB color value
				const { endIndex, result } = parseDictionaryOrArray(tokens, i + 4);
				i = endIndex;
				valueParsed = result;
				break;
			}
			default: {
				// That's a primitive
				valueParsed = valueToParse;
				i += 3;
				break;
			}
		}

		const currentValue = dictionary[key];
		if (currentValue === undefined) {
			// There is no other key already there, just add the value
			dictionary[key] = valueParsed;
		} else if (Array.isArray(currentValue)) {
			// This is an array, add the value to the array
			currentValue.push(valueParsed);
		} else {
			// There is already something that is not an array, created an array with both values
			dictionary[key] = [currentValue, valueParsed];
		}
	}

	// We should never reach here
	return {
		endIndex: tokens.length,
		result: dictionary,
	};
}

export function parse(tokens: string[]): Save {
	return (parseDictionary(tokens, 0).result as unknown) as Save;
}
