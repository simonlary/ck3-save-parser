/* eslint-disable @typescript-eslint/no-explicit-any */
export interface Save {
	meta_data: MetaData;
	ironman_manager: IronmanManager;
	date: string;
	bookmark_date: string;
	first_start: FirstStart;
	speed: string;
	random_seed: string;
	random_count: string;
	variables: SaveVariables;
	game_rules: SaveGameRules;
	provinces: { [key: string]: Province };
	landed_titles: LandedTitles;
	dynasties: Dynasties;
	deleted_characters: string[];
	living: { [key: string]: Living };
	dead_unprunable: { [key: string]: DeadUnprunable };
	characters: Characters;
	character_lookup: { [key: string]: string };
	units: { [key: string]: UnitClass | SexualityValue };
	activities: { [key: string]: ActivityClass | SexualityValue };
	opinions: Opinions;
	relations: Relations;
	schemes: Schemes;
	stories: Stories;
	pending_character_interactions: PendingCharacterInteractions;
	secrets: Secrets;
	armies: Armies;
	combats: Combats;
	vassal_contracts: VassalContracts;
	religion: SaveFileReligion;
	wars: Wars;
	sieges: Sieges;
	raid: RAID;
	succession: any[];
	holdings: Holdings;
	ai: AI;
	county_manager: CountyManager;
	fleet_manager: FleetManager;
	council_task_manager: CouncilTaskManager;
	important_action_manager: ImportantActionManager;
	faction_manager: FactionManager;
	culture_manager: CultureManager;
	mercenary_company_manager: MercenaryCompanyManager;
	holy_orders: HolyOrders;
	coat_of_arms: CoatOfArms;
	triggered_event: TriggeredEvent[];
	next_player_event_id: string;
	played_character: PlayedCharacter;
	currently_played_characters: string[];
}

export interface ActivityClass {
	type: PurpleType;
	location: string;
	active?: FirstStart;
	variables?: ActivityVariables;
	expiration_date?: string;
	spawn_date: string;
	owner?: string;
	accepted?: string[];
	declined?: string[];
	completed?: FirstStart;
	invitations?: string[];
	expiration_days?: string;
}

export enum FirstStart {
	No = "no",
	Yes = "yes",
}

export enum PurpleType {
	ActivityFeast = "activity_feast",
	ActivityHunt = "activity_hunt",
	ActivityMeditation = "activity_meditation",
	ActivityPilgrimage = "activity_pilgrimage",
}

export interface ActivityVariables {
	data?: PurpleDatum[];
	list?: PurpleList[];
}

export interface PurpleDatum {
	flag: string;
	data: Special;
	tick?: string;
}

export interface Special {
	type: SpecialType;
	identity?: string;
}

export enum SpecialType {
	Act = "act",
	Boolean = "boolean",
	Cb = "cb",
	Char = "char",
	Culture = "culture",
	Fac = "fac",
	Faith = "faith",
	Flag = "flag",
	Ho = "ho",
	Lt = "lt",
	Mc = "mc",
	Prov = "prov",
	SEC = "sec",
	Scheme = "scheme",
	Value = "value",
	War = "war",
}

export interface PurpleList {
	name: PurpleName;
	item: Special[];
}

export enum PurpleName {
	FinishedParticipants = "finished_participants",
	PotentialHuntParticipants = "potential_hunt_participants",
}

export enum SexualityValue {
	As = "as",
	BI = "bi",
	Ho = "ho",
	None = "none",
}

export interface AI {
	war_coordinator_db: { [key: string]: WarCoordinatorDBClass | SexualityValue };
	war_plan_db: { [key: string]: WarPlanDBClass | SexualityValue };
	ai_strategies: { [key: string]: AIStrategy };
}

export interface AIStrategy {
	active: FirstStart;
	initialized: FirstStart;
	advanced_ai_data: AdvancedAIData;
}

export interface AdvancedAIData {
	threat?: DeJureOverlapElement[] | DeJureOverlapElement;
	friend?: DeJureOverlapElement[] | DeJureOverlapElement;
	cb_target?: string[];
	ai_goal_construct_holding?: AIGoalConstructHoldingElement[] | AIGoalConstructHoldingElement;
	budget_reserved: string;
	budget_war_chest: string;
	budget_long_term: string;
	budget_short_term: string;
	desired_war_chest?: string;
	last_raise_levies_date?: string;
	hire_mercenary?: FirstStart;
	ticks: string[];
	enemy?: DeJureOverlapElement[] | DeJureOverlapElement;
	de_jure_overlap?: DeJureOverlapElement[] | DeJureOverlapElement;
	last_raid?: string;
	war_plans?: string[];
	ai_goal_create_title?: AIGoalCreateTitleElement[] | AIGoalCreateTitleElement;
	check_raid?: FirstStart;
	min_reserved_gold?: string;
	last_counter_raid?: string;
	ai_goal_decision?: AIGoalDecision;
}

export interface AIGoalConstructHoldingElement {
	priority: string;
	province: string;
	building: BuildingEnum;
}

export enum BuildingEnum {
	Castle01 = "castle_01",
	City01 = "city_01",
	Temple01 = "temple_01",
}

export interface AIGoalCreateTitleElement {
	priority: string;
	title: string;
}

export interface AIGoalDecision {
	priority: string;
	decision: string;
}

export interface DeJureOverlapElement {
	id: string;
	value: string;
}

export interface WarCoordinatorDBClass {
	war: string;
	war_stance?: WarStance;
	supply_limit: string;
	garrison_limit: string;
	stack_size: string;
	participants: string[];
	units?: string[];
	next_unit_stack_id: string;
	update_war_stance_tick: string;
	update_split_merge_tick: string;
	update_targets_tick: string;
	center: string[];
	radius: string;
	unit_stack?: UnitStackElement[] | PurpleUnitStack;
	attacker?: FirstStart;
	can_counter_invade?: FirstStart;
	is_desperate?: FirstStart;
}

export interface UnitStackElement {
	id: string;
	next_subunit_stack_id: string;
	units: string[];
	supply_check_days?: string;
	order_province?: string;
	order_type?: OrderType;
	order_score?: string;
	subunit_stack: PurpleSubunitStack[] | FluffySubunitStack;
	idle_days?: string;
	is_merging?: FirstStart;
	target_unit_stack_id?: string;
	is_retreating?: FirstStart;
}

export enum OrderType {
	CapitalProvince = "capital_province",
	DefendWargoalProvince = "defend_wargoal_province",
	EnemyAllyProvince = "enemy_ally_province",
	EnemyCapitalProvince = "enemy_capital_province",
	EnemyProvince = "enemy_province",
	EnemyUnitProvince = "enemy_unit_province",
	Province = "province",
	WargoalProvince = "wargoal_province",
}

export interface PurpleSubunitStack {
	id: string;
	units: string[];
	task_province?: string;
	needs_help?: FirstStart;
	is_helping?: FirstStart;
}

export interface FluffySubunitStack {
	id: string;
	units: string[];
}

export interface PurpleUnitStack {
	id: string;
	next_subunit_stack_id: string;
	units: string[];
	supply_check_days?: string;
	order_province?: string;
	order_type?: OrderType;
	order_score?: string;
	subunit_stack: TentacledSubunitStack[] | FluffySubunitStack;
	idle_days?: string;
	stand_and_fight?: string;
	stand_and_cooldown?: string;
}

export interface TentacledSubunitStack {
	id: string;
	units: string[];
	task_province?: string;
	is_organizing?: FirstStart;
}

export enum WarStance {
	AttackerDefensive = "attacker_defensive",
	AttackerOffensive = "attacker_offensive",
	DefenderDefensive = "defender_defensive",
	DefenderOffensive = "defender_offensive",
}

export interface WarPlanDBClass {
	war: string;
	war_coordinator: string;
	owner: string;
	strength?: string;
	allied_strength?: string;
	enemy_strength: string;
	units?: string[];
	importance?: string;
	emergency?: string;
}

export interface Armies {
	regiments: { [key: string]: RegimentClass | SexualityValue };
	army_regiments: { [key: string]: ArmyRegimentClass | SexualityValue };
	armies: { [key: string]: ArmyClass | SexualityValue };
	gathering_armies: string[];
	name_manager: NameManager;
}

export interface ArmyClass {
	regiments: string[];
	commander?: string;
	unit: string;
	cost?: Cost;
	supply: string;
	last_supply_date: string;
	gathered_date: string;
	name: NameClass;
	embark_fleet?: string;
	disembark_penalty_days?: string;
	raid_army?: FirstStart;
	raid_gold?: string;
	looter?: FirstStart;
	variables?: ArmyVariables;
	gathering_groups?: GatheringGroup[];
}

export interface Cost {
	gold?: string;
	prestige?: string;
}

export interface GatheringGroup {
	regiments: RegimentElement[];
	date: string;
}

export interface RegimentElement {
	regiment: string;
	index?: string;
}

export interface NameClass {
	id?: string;
	province?: string;
	name?: string;
}

export interface ArmyVariables {
	data: FluffyDatum[];
}

export interface FluffyDatum {
	flag: string;
	data: Special;
}

export interface ArmyRegimentClass {
	chunks?: RegimentElement[];
	cached: Cached;
	army: string;
	source?: Source;
	vassal?: string;
	knight?: string;
	type?: ArmyRegimentType;
}

export interface Cached {
	current?: string;
	max?: string;
	size?: string;
	levies?: Levies;
	men_at_arms?: Cost;
}

export interface Levies {
	gold: string;
}

export enum Source {
	Event = "event",
	Hired = "hired",
}

export enum ArmyRegimentType {
	Abudrar = "abudrar",
	ArmoredFootmen = "armored_footmen",
	ArmoredHorsemen = "armored_horsemen",
	Ayyar = "ayyar",
	Bombard = "bombard",
	Bowmen = "bowmen",
	BushHunter = "bush_hunter",
	Caballero = "caballero",
	CamelRider = "camel_rider",
	Cataphract = "cataphract",
	ChuKoNu = "chu_ko_nu",
	Crossbowmen = "crossbowmen",
	Druzhina = "druzhina",
	Gendarme = "gendarme",
	Goedendag = "goedendag",
	GuineaWarrior = "guinea_warrior",
	Hobelar = "hobelar",
	HornWarrior = "horn_warrior",
	HorseArchers = "horse_archers",
	HouseGuard = "house_guard",
	Huscarl = "huscarl",
	Hussar = "hussar",
	Khandayat = "khandayat",
	Landsknecht = "landsknecht",
	LightFootmen = "light_footmen",
	LightHorsemen = "light_horsemen",
	Longbowmen = "longbowmen",
	Mangonel = "mangonel",
	Metsanvartija = "metsanvartija",
	Mountaineer = "mountaineer",
	Mubarizun = "mubarizun",
	Onager = "onager",
	PalaceGuards = "palace_guards",
	Picchieri = "picchieri",
	PikemenUnit = "pikemen_unit",
	SahelHorsemen = "sahel_horsemen",
	Schiltron = "schiltron",
	TeutonicKnights = "teutonic_knights",
	Trebuchet = "trebuchet",
	WarElephant = "war_elephant",
	Zbrojnosh = "zbrojnosh",
}

export interface NameManager {
	cached_army_index_map: { [key: string]: string[] };
}

export interface RegimentClass {
	origin?: string;
	max?: string;
	chunks?: RegimentChunk[];
	size?: string;
	source?: Source;
	type?: ArmyRegimentType;
	owner?: string;
	war?: string;
	inheritable?: FirstStart;
	uses_supply?: FirstStart;
}

export interface RegimentChunk {
	size?: string;
	max?: string;
	current?: string;
	is_raising?: FirstStart;
}

export interface Characters {
	dead_prunable: { [key: string]: DeadPrunable };
	prune_queue: any[];
	dummy_female: DummyFemale;
	dummy_male: DummyMale;
	unborn: Unborn[];
	unborn_birth: Unborn[];
	natural_deaths: NaturalDeaths;
	current_natural_death: string;
	betrothal_timeout_queue: string[];
	sexuality_chances: SexualityChances;
}

export interface DeadPrunable {
	first_name: string;
	birth: string;
	female?: FirstStart;
	culture?: string;
	faith?: string;
	skill: string[];
	prowess_age?: string;
	mass?: string;
	traits?: string[];
	family_data?: any[] | PurpleFamilyData;
	dead_data: DeadPrunableDeadData;
	dna?: string;
	recessive_traits?: string[];
	sexuality?: SexualityValue;
	dynasty_house?: string;
	nickname?: Nickname;
	inactive_traits?: string[];
}

export interface DeadPrunableDeadData {
	date: string;
	reason?: Reason;
	liege?: string;
	liege_title?: string;
	kills?: string[];
	domain?: string[];
	government?: Government;
	killer?: string;
	know_of_killer?: string[];
	named_title?: string;
	life_expectancy?: string;
	killer_known?: FirstStart;
}

export enum Government {
	ClanGovernment = "clan_government",
	FeudalGovernment = "feudal_government",
	HolyOrderGovernment = "holy_order_government",
	MercenaryGovernment = "mercenary_government",
	RepublicGovernment = "republic_government",
	TheocracyGovernment = "theocracy_government",
	TribalGovernment = "tribal_government",
}

export enum Reason {
	Blind = "blind",
	DeathAccident = "death_accident",
	DeathAttemptedTreatment = "death_attempted_treatment",
	DeathBattle = "death_battle",
	DeathBleeder = "death_bleeder",
	DeathBubonicPlague = "death_bubonic_plague",
	DeathCancer = "death_cancer",
	DeathCarpAccident = "death_carp_accident",
	DeathChildbirth = "death_childbirth",
	DeathConsumption = "death_consumption",
	DeathDepressed = "death_depressed",
	DeathDisappearance = "death_disappearance",
	DeathDrinkingPassive = "death_drinking_passive",
	DeathDuel = "death_duel",
	DeathDungeon = "death_dungeon",
	DeathDungeonPassive = "death_dungeon_passive",
	DeathEaten = "death_eaten",
	DeathExecution = "death_execution",
	DeathFight = "death_fight",
	DeathFlagellant = "death_flagellant",
	DeathGiant = "death_giant",
	DeathGoutRidden = "death_gout_ridden",
	DeathGreatPox = "death_great_pox",
	DeathHeadRippedOff = "death_head_ripped_off",
	DeathHorseRidingAccident = "death_horse_riding_accident",
	DeathHuntingAccident = "death_hunting_accident",
	DeathIll = "death_ill",
	DeathInbred = "death_inbred",
	DeathIncapable = "death_incapable",
	DeathLeper = "death_leper",
	DeathLunatic = "death_lunatic",
	DeathMaimed = "death_maimed",
	DeathMalnourishment = "death_malnourishment",
	DeathMurder = "death_murder",
	DeathMurderKnown = "death_murder_known",
	DeathMysterious = "death_mysterious",
	DeathNaturalCauses = "death_natural_causes",
	DeathObesity = "death_obesity",
	DeathOldAge = "death_old_age",
	DeathPhysicianMistreatment = "death_physician_mistreatment",
	DeathPhysiqueBad1 = "death_physique_bad_1",
	DeathPhysiqueBad2 = "death_physique_bad_2",
	DeathPhysiqueBad3 = "death_physique_bad_3",
	DeathPlotting = "death_plotting",
	DeathPneumonic = "death_pneumonic",
	DeathPoison = "death_poison",
	DeathPossessed = "death_possessed",
	DeathPunishment = "death_punishment",
	DeathRAID = "death_raid",
	DeathSickly = "death_sickly",
	DeathSiege = "death_siege",
	DeathSmallpox = "death_smallpox",
	DeathSpindly = "death_spindly",
	DeathStress = "death_stress",
	DeathSuicide = "death_suicide",
	DeathTorture = "death_torture",
	DeathTorturePassive = "death_torture_passive",
	DeathTreatment = "death_treatment",
	DeathTyphus = "death_typhus",
	DeathVanished = "death_vanished",
	DeathWheezing = "death_wheezing",
	DeathWhipping = "death_whipping",
	DeathWounded1 = "death_wounded_1",
	DeathWounded2 = "death_wounded_2",
	DeathWounded3 = "death_wounded_3",
	DeathWounds = "death_wounds",
}

export interface PurpleFamilyData {
	child?: string[];
	primary_spouse?: string;
	spouse?: string[] | string;
	former_spouses?: string[];
	real_father?: string;
}

export enum Nickname {
	NickTheBully = "nick_the_bully",
	NickTheFlayer = "nick_the_flayer",
	NickTheFoolish = "nick_the_foolish",
	NickTheFury = "nick_the_fury",
	NickTheGardener = "nick_the_gardener",
	NickTheHeartbreaker = "nick_the_heartbreaker",
	NickTheImperious = "nick_the_imperious",
	NickThePhilosopher = "nick_the_philosopher",
	NickThePoet = "nick_the_poet",
	NickTheScholar = "nick_the_scholar",
}

export interface DummyFemale {
	"0": The0;
}

export interface The0 {
	first_name: string;
	birth: string;
	female?: FirstStart;
	culture: string;
	skill: string[];
	sexuality: SexualityValue;
	alive_data: The0_AliveData;
}

export interface The0_AliveData {
	fertility: string;
	health: string;
	piety: PurplePiety;
	prestige: PurplePiety;
}

export interface PurplePiety {
	accumulated: string;
}

export interface DummyMale {
	"1": The0;
}

export interface NaturalDeaths {
	"1": { [key: string]: string };
	"2": { [key: string]: string };
	"3": { [key: string]: string };
	"4": { [key: string]: string };
	"5": { [key: string]: string };
	"6": The6;
	"7": The7;
	"8": { [key: string]: string };
	"9": { [key: string]: string };
	"22": { [key: string]: string };
	"23": The23;
	"24": { [key: string]: string };
	"25": { [key: string]: string };
	"26": { [key: string]: string };
	"27": { [key: string]: string };
	"28": The28;
	"29": The29;
	size: string;
}

export interface The23 {
	"67187362": string;
}

export interface The28 {
	"67192004": string;
}

export interface The29 {
	"16897797": string;
}

export interface The6 {
	"33654069": string;
}

export interface The7 {
	"122342": string;
}

export interface SexualityChances {
	he: string;
	ho: string;
	bi: string;
	as: string;
}

export interface Unborn {
	mother: string;
	father: string;
	assumed_father: string;
	date: string;
	matrilineal?: FirstStart;
	count?: string;
	known_bastard?: FirstStart;
}

export interface CoatOfArms {
	coat_of_arms_manager_name_map: { [key: string]: string };
	coat_of_arms_manager_database: { [key: string]: CoatOfArmsManagerDatabase };
	next_id: string;
}

export interface CoatOfArmsManagerDatabase {
	pattern?: Pattern;
	color1?: Color;
	color2?: Color;
	textured_emblem?: TexturedEmblem;
	colored_emblem?: PurpleColoredEmblem[] | FluffyColoredEmblem;
	color3?: Color;
	color4?: Color;
	sub?: CoatOfArmsManagerDatabaseSub[];
}

export enum Color {
	Black = "black",
	Blue = "blue",
	BlueLight = "blue_light",
	Brown = "brown",
	Green = "green",
	GreenLight = "green_light",
	Grey = "grey",
	Orange = "orange",
	Purple = "purple",
	Red = "red",
	White = "white",
	Yellow = "yellow",
	YellowLight = "yellow_light",
}

export interface PurpleColoredEmblem {
	color1: Color;
	color2?: Color;
	texture: string;
	instance?: Array<any[] | PurpleInstance> | PurpleInstance;
	mask?: string[];
}

export interface PurpleInstance {
	position?: string[];
	scale?: string[];
	rotation?: string;
}

export interface FluffyColoredEmblem {
	color1: Color;
	color2?: Color;
	texture: string;
	instance?: PurpleInstance[] | PurpleInstance;
	mask?: string[];
}

export enum Pattern {
	PatternBarruly08Dds = "pattern_barruly_08.dds",
	PatternBarruly10Dds = "pattern_barruly_10.dds",
	PatternBarrulySpecialDds = "pattern_barruly_special.dds",
	PatternBend01Dds = "pattern_bend_01.dds",
	PatternCheckers01Dds = "pattern_checkers_01.dds",
	PatternCheckers02Dds = "pattern_checkers_02.dds",
	PatternCheckers08Dds = "pattern_checkers_08.dds",
	PatternCheckers12Dds = "pattern_checkers_12.dds",
	PatternCheckersDiagonal01Dds = "pattern_checkers_diagonal_01.dds",
	PatternChiefDds = "pattern_chief.dds",
	PatternCross01Dds = "pattern_cross_01.dds",
	PatternCross02Dds = "pattern_cross_02.dds",
	PatternDiagonalSplit01Dds = "pattern_diagonal_split_01.dds",
	PatternDiagonalSplit02Dds = "pattern_diagonal_split_02.dds",
	PatternHorizontalBar01Dds = "pattern_horizontal_bar_01.dds",
	PatternHorizontalSplit01Dds = "pattern_horizontal_split_01.dds",
	PatternHorizontalSplit02Dds = "pattern_horizontal_split_02.dds",
	PatternHorizontalStripes01Dds = "pattern_horizontal_stripes_01.dds",
	PatternQuarterDds = "pattern_quarter.dds",
	PatternShield01Dds = "pattern_shield_01.dds",
	PatternShield03_SmallDds = "pattern_shield_03_small.dds",
	PatternSolidDds = "pattern_solid.dds",
	PatternTricolorBend01Dds = "pattern_tricolor_bend_01.dds",
	PatternTricolorBend02Dds = "pattern_tricolor_bend_02.dds",
	PatternTricolorHorizontal01Dds = "pattern_tricolor_horizontal_01.dds",
	PatternVerticalSplit01Dds = "pattern_vertical_split_01.dds",
	PatternVerticalStripes01Dds = "pattern_vertical_stripes_01.dds",
	PatternVerticalStripes02Dds = "pattern_vertical_stripes_02.dds",
	PatternWaves01Dds = "pattern_waves_01.dds",
	TempPatternBendySpecialDds = "temp_pattern_bendy_special.dds",
}

export interface CoatOfArmsManagerDatabaseSub {
	instance: SubInstance;
	pattern?: Pattern;
	color1?: Color;
	color2?: Color;
	colored_emblem?: FluffyColoredEmblem[] | FluffyColoredEmblem;
	color3?: Color;
	color4?: Color;
}

export interface SubInstance {
	scale: string[];
	offset?: string[];
}

export interface TexturedEmblem {
	texture: string;
}

export interface Combats {
	combat_results: { [key: string]: CombatResultClass | SexualityValue };
	combats: { [key: string]: CombatClass | SexualityValue };
}

export interface CombatResultClass {
	location: string;
	attacker: CombatResultAttacker;
	defender: CombatResultAttacker;
	winning_side: string;
	start_date: string;
	end_date: string;
	win: string;
	event?: EventElement[] | EventElement;
}

export interface CombatResultAttacker {
	main_participant: string;
	commander: string;
	inital_soldiers: string;
	surviving_soldiers: string;
	participants: string[];
	regiment_stats: RegimentStat[];
}

export interface RegimentStat {
	knight: string;
	damage_last_tick: string;
	main_kills: string;
	pursuit_kills: string;
	main_losses: string;
	pursuit_losses_maa: string;
	pursuit_losses_self: string;
	initial_count: string;
	final_count: string;
	type?: ArmyRegimentType;
}

export interface EventElement {
	left_portrait: string;
	right_portrait: string;
	key: string;
	type: string;
	attacker?: FirstStart;
}

export interface CombatClass {
	attacker: CombatAttacker;
	defender: CombatAttacker;
	phase: string;
	combat_results: string;
	province: string;
	combat_width: string;
	base_combat_width: string;
	winning_side: string;
	days_since_last_action: string;
	advantage: string;
	attacker_roll: string;
	defender_roll: string;
	advantage_damage_factor: string;
	days: string;
	levy_soft_casualties?: string;
	maa_soft_casualties?: string;
}

export interface CombatAttacker {
	armies: string[];
	men_at_arms?: Levy[];
	levies: Levy[];
	commander: string;
	leader: string;
	combat_effects?: CombatEffect[];
	total_fighting_men: string;
	total_levy_men: string;
	initial_men: string;
	initial_levies: string;
	character?: Character;
}

export interface Character {
	character: string;
	contribution: string;
}

export interface CombatEffect {
	combat_effect: string;
	advantage_change: string;
}

export interface Levy {
	regiment: string;
	starting: string;
	current: string;
	soft_casualties: string;
}

export interface CouncilTaskManager {
	active: { [key: string]: PurpleActive | SexualityValue };
}

export interface PurpleActive {
	type: FluffyType;
	owner?: string;
	court_owner: string;
	last_councillor_change?: string;
	last_appointed_councillor?: string;
	target?: Special;
	frozen?: FirstStart;
	progress?: string;
}

export enum FluffyType {
	TaskCollectTaxes = "task_collect_taxes",
	TaskConversion = "task_conversion",
	TaskDevelopCounty = "task_develop_county",
	TaskDisruptSchemes = "task_disrupt_schemes",
	TaskDomesticAffairs = "task_domestic_affairs",
	TaskFabricateClaim = "task_fabricate_claim",
	TaskFindSecrets = "task_find_secrets",
	TaskForeignAffairs = "task_foreign_affairs",
	TaskIncreaseControl = "task_increase_control",
	TaskIntegrateTitle = "task_integrate_title",
	TaskOrganizeLevies = "task_organize_levies",
	TaskPromoteCulture = "task_promote_culture",
	TaskReligiousRelations = "task_religious_relations",
	TaskSpouseDefault = "task_spouse_default",
	TaskTrainCommanders = "task_train_commanders",
}

export interface CountyManager {
	counties: { [key: string]: County };
	monthly_increase: string[];
}

export interface County {
	development: string;
	development_progress: string;
	council_tasks?: string[];
	county_control: string;
	culture: string;
	faith: string;
	timed_modifier?: TimedModifierElement[] | TimedModifierElement;
}

export interface TimedModifierElement {
	modifier: string;
	expiration_date: string;
}

export interface CultureManager {
	cultures: { [key: string]: Culture };
	template_cultures: string[];
	era_discovery: EraDiscovery[];
}

export interface Culture {
	culture_template: string;
	culture_era_data: CultureEraDatum[];
	culture_innovation: CultureInnovation[];
	exposure_marker?: ExposureMarker;
	fascination_marker?: ExposureMarker;
	culture_marker?: string;
	exposure_type?: ExposureType;
	head?: string;
}

export interface CultureEraDatum {
	type: CultureEra;
	progress?: string;
	left?: string;
	join?: string;
}

export enum CultureEra {
	CultureEraEarlyMedieval = "culture_era_early_medieval",
	CultureEraHighMedieval = "culture_era_high_medieval",
	CultureEraLateMedieval = "culture_era_late_medieval",
	CultureEraTribal = "culture_era_tribal",
}

export interface CultureInnovation {
	type: ExposureMarker;
	progress?: string;
}

export enum ExposureMarker {
	InnovationAdaptiveMilitia = "innovation_adaptive_militia",
	InnovationAdvancedBowmaking = "innovation_advanced_bowmaking",
	InnovationAfricanCanoes = "innovation_african_canoes",
	InnovationAlpineSupremacy = "innovation_alpine_supremacy",
	InnovationArchedSaddle = "innovation_arched_saddle",
	InnovationArmilarySphere = "innovation_armilary_sphere",
	InnovationBaliffs = "innovation_baliffs",
	InnovationBannus = "innovation_bannus",
	InnovationBarracks = "innovation_barracks",
	InnovationBattlements = "innovation_battlements",
	InnovationBombard = "innovation_bombard",
	InnovationBurhs = "innovation_burhs",
	InnovationBushHunting = "innovation_bush_hunting",
	InnovationCaballeros = "innovation_caballeros",
	InnovationCastleBaileys = "innovation_castle_baileys",
	InnovationCasusBelli = "innovation_casus_belli",
	InnovationCataphracts = "innovation_cataphracts",
	InnovationCatapult = "innovation_catapult",
	InnovationChronicleWriting = "innovation_chronicle_writing",
	InnovationCityPlanning = "innovation_city_planning",
	InnovationCompoundBows = "innovation_compound_bows",
	InnovationCondottieri = "innovation_condottieri",
	InnovationCourtOfficials = "innovation_court_officials",
	InnovationCranes = "innovation_cranes",
	InnovationCropRotation = "innovation_crop_rotation",
	InnovationCurrency01 = "innovation_currency_01",
	InnovationCurrency02 = "innovation_currency_02",
	InnovationCurrency03 = "innovation_currency_03",
	InnovationCurrency04 = "innovation_currency_04",
	InnovationDeccanUnity = "innovation_deccan_unity",
	InnovationDesertMountainHerding = "innovation_desert_mountain_herding",
	InnovationDesertTactics = "innovation_desert_tactics",
	InnovationDevelopment01 = "innovation_development_01",
	InnovationDevelopment02 = "innovation_development_02",
	InnovationDevelopment03 = "innovation_development_03",
	InnovationDevelopment04 = "innovation_development_04",
	InnovationDivineRight = "innovation_divine_right",
	InnovationDruzhina = "innovation_druzhina",
	InnovationEastSettling = "innovation_east_settling",
	InnovationElephantry = "innovation_elephantry",
	InnovationErmineCloaks = "innovation_ermine_cloaks",
	InnovationForestWardens = "innovation_forest_wardens",
	InnovationFrenchPeerage = "innovation_french_peerage",
	InnovationFutuwaa = "innovation_futuwaa",
	InnovationGavelkind = "innovation_gavelkind",
	InnovationGhilman = "innovation_ghilman",
	InnovationGuilds = "innovation_guilds",
	InnovationHIRD = "innovation_hird",
	InnovationHeraldry = "innovation_heraldry",
	InnovationHereditaryRule = "innovation_hereditary_rule",
	InnovationHoardings = "innovation_hoardings",
	InnovationHobbies = "innovation_hobbies",
	InnovationHorseshoes = "innovation_horseshoes",
	InnovationHouseSoldiers = "innovation_house_soldiers",
	InnovationHussarRaids = "innovation_hussar_raids",
	InnovationKhandayats = "innovation_khandayats",
	InnovationKnighthood = "innovation_knighthood",
	InnovationLandGrants = "innovation_land_grants",
	InnovationLedger = "innovation_ledger",
	InnovationLegionnaires = "innovation_legionnaires",
	InnovationLongboats = "innovation_longboats",
	InnovationLongbows = "innovation_longbows",
	InnovationMachicolations = "innovation_machicolations",
	InnovationMangonel = "innovation_mangonel",
	InnovationManorialism = "innovation_manorialism",
	InnovationMenAtArms = "innovation_men_at_arms",
	InnovationMobileGuards = "innovation_mobile_guards",
	InnovationMotte = "innovation_motte",
	InnovationMountainSkirmishing = "innovation_mountain_skirmishing",
	InnovationMubarizuns = "innovation_mubarizuns",
	InnovationMuladi = "innovation_muladi",
	InnovationMusteringGrounds = "innovation_mustering_grounds",
	InnovationNoblesseOblige = "innovation_noblesse_oblige",
	InnovationPikeColumns = "innovation_pike_columns",
	InnovationPlateArmor = "innovation_plate_armor",
	InnovationPlenaryAssemblies = "innovation_plenary_assemblies",
	InnovationPrimogeniture = "innovation_primogeniture",
	InnovationQuiltedArmor = "innovation_quilted_armor",
	InnovationReconquista = "innovation_reconquista",
	InnovationRectilinearSchiltron = "innovation_rectilinear_schiltron",
	InnovationRepeatingCrossbow = "innovation_repeating_crossbow",
	InnovationRightfulOwnership = "innovation_rightful_ownership",
	InnovationRoyalArmory = "innovation_royal_armory",
	InnovationRoyalArmyTradition = "innovation_royal_army_tradition",
	InnovationRoyalPrerogative = "innovation_royal_prerogative",
	InnovationSahelHorsemen = "innovation_sahel_horsemen",
	InnovationSappers = "innovation_sappers",
	InnovationSarawit = "innovation_sarawit",
	InnovationScutage = "innovation_scutage",
	InnovationSeigneurialism = "innovation_seigneurialism",
	InnovationStandingArmies = "innovation_standing_armies",
	InnovationStemDuchies = "innovation_stem_duchies",
	InnovationTableOfPrinces = "innovation_table_of_princes",
	InnovationTrebuchet = "innovation_trebuchet",
	InnovationUplandSkirmishing = "innovation_upland_skirmishing",
	InnovationValets = "innovation_valets",
	InnovationVisigothicCodes = "innovation_visigothic_codes",
	InnovationWarCamels = "innovation_war_camels",
	InnovationWierdijks = "innovation_wierdijks",
	InnovationWindmills = "innovation_windmills",
	InnovationWootzSteel = "innovation_wootz_steel",
	InnovationZbrojnosh = "innovation_zbrojnosh",
	InnovationZweihanders = "innovation_zweihanders",
}

export enum ExposureType {
	Realm = "realm",
	Religion = "religion",
}

export interface EraDiscovery {
	culture_era: CultureEra;
	culture?: string;
}

export interface DeadUnprunable {
	first_name: string;
	birth: string;
	was_playable?: FirstStart;
	culture?: string;
	faith?: string;
	dynasty_house?: string;
	skill: string[];
	mass?: string;
	traits?: string[];
	family_data?: any[] | PurpleFamilyData;
	dead_data: DeadPrunableDeadData;
	regnal_name?: string;
	nickname?: string;
	prowess_age?: string;
	sexuality?: SexualityValue;
	female?: FirstStart;
	dna?: string;
	recessive_traits?: string[];
	inactive_traits?: string[];
}

export interface Dynasties {
	dynasty_house: { [key: string]: DynastyHouse };
	dynasties: { [key: string]: DynastyClass | SexualityValue };
	static_dynasties: string[];
	static_dynasty_houses: string[];
}

export interface DynastyClass {
	key?: string;
	prestige: DynastyPiety;
	coat_of_arms_id?: string;
	update_day: string;
	good_for_realm_name?: FirstStart;
	dynasty_head?: string;
	perk?: Perk[];
	variables?: ArmyVariables;
	timed_modifier?: TimedModifierElement;
	name?: string;
	localized_name?: string;
	prefix?: Prefix;
}

export enum Perk {
	BloodLegacy1 = "blood_legacy_1",
	BloodLegacy2 = "blood_legacy_2",
	BloodLegacy3 = "blood_legacy_3",
	BloodLegacy4 = "blood_legacy_4",
	BloodLegacy5 = "blood_legacy_5",
	EruditionLegacy1 = "erudition_legacy_1",
	EruditionLegacy2 = "erudition_legacy_2",
	EruditionLegacy3 = "erudition_legacy_3",
	EruditionLegacy4 = "erudition_legacy_4",
	EruditionLegacy5 = "erudition_legacy_5",
	GloryLegacy1 = "glory_legacy_1",
	GloryLegacy2 = "glory_legacy_2",
	GloryLegacy3 = "glory_legacy_3",
	GloryLegacy4 = "glory_legacy_4",
	GloryLegacy5 = "glory_legacy_5",
	GuileLegacy1 = "guile_legacy_1",
	GuileLegacy2 = "guile_legacy_2",
	GuileLegacy3 = "guile_legacy_3",
	GuileLegacy4 = "guile_legacy_4",
	GuileLegacy5 = "guile_legacy_5",
	KinLegacy1 = "kin_legacy_1",
	KinLegacy2 = "kin_legacy_2",
	KinLegacy3 = "kin_legacy_3",
	KinLegacy4 = "kin_legacy_4",
	KinLegacy5 = "kin_legacy_5",
	LawLegacy1 = "law_legacy_1",
	LawLegacy2 = "law_legacy_2",
	LawLegacy3 = "law_legacy_3",
	LawLegacy4 = "law_legacy_4",
	LawLegacy5 = "law_legacy_5",
	WarfareLegacy1 = "warfare_legacy_1",
	WarfareLegacy2 = "warfare_legacy_2",
	WarfareLegacy3 = "warfare_legacy_3",
	WarfareLegacy4 = "warfare_legacy_4",
	WarfareLegacy5 = "warfare_legacy_5",
}

export enum Prefix {
	Dynnp = "dynnp__",
	DynnpA = "dynnp_a",
	DynnpAF = "dynnp_af",
	DynnpAV = "dynnp_av",
	DynnpAd = "dynnp_ad-",
	DynnpAl = "dynnp_al-",
	DynnpAn = "dynnp_an",
	DynnpAp = "dynnp_ap",
	DynnpAs = "dynnp_as-",
	DynnpBanu = "dynnp_banu",
	DynnpD = "dynnp_d_",
	DynnpDES = "dynnp_des",
	DynnpDIN = "dynnp_din",
	DynnpDa = "dynnp_da",
	DynnpDalle = "dynnp_dalle",
	DynnpDe = "dynnp_de",
	DynnpDegli = "dynnp_degli",
	DynnpDei = "dynnp_dei",
	DynnpDel = "dynnp_del",
	DynnpDell = "dynnp_dell_",
	DynnpDella = "dynnp_della",
	DynnpDi = "dynnp_di",
	DynnpDu = "dynnp_du",
	DynnpEl = "dynnp_el-",
	DynnpGada = "dynnp_gada",
	DynnpI = "dynnp_i-",
	DynnpIs = "dynnp_is",
	DynnpKohta = "dynnp_kohta",
	DynnpLE = "dynnp_le",
	DynnpLa = "dynnp_la",
	DynnpMAC = "dynnp_mac",
	DynnpMn = "dynnp_mn",
	DynnpNa = "dynnp_na",
	DynnpOd = "dynnp_od",
	DynnpOf = "dynnp_of",
	DynnpOt = "dynnp_ot",
	DynnpTon = "dynnp_ton",
	DynnpUI = "dynnp_ui",
	DynnpUa = "dynnp_ua",
	DynnpVan = "dynnp_van",
	DynnpVe = "dynnp_ve",
	DynnpVon = "dynnp_von",
	DynnpZ = "dynnp_z",
	DynnpZe = "dynnp_ze",
	DynnpZu = "dynnp_zu",
	DynnpatPreMc = "dynnpat_pre_Mc",
	PrefixDynnpA = "dynnp_a_",
	PrefixDynnpAn = "dynnp_an-",
}

export interface DynastyPiety {
	currency?: string;
	accumulated: string;
}

export interface DynastyHouse {
	name?: string;
	found_date: string;
	dynasty: string;
	historical?: string[];
	motto: MottoClass | string;
	prefix?: Prefix;
	head_of_house?: string;
	forced_coa_religiongroup?: ForcedCoaReligiongroup;
	timed_modifier?: TimedModifierElement[] | TimedModifierElement;
	key?: string;
	coat_of_arms_id?: string;
	localized_name?: string;
	parent_dynasty_house?: string;
}

export enum ForcedCoaReligiongroup {
	Christian = "christian",
	Muslim = "muslim",
	ZoroastrianGroup = "zoroastrian_group",
}

export interface MottoClass {
	key: Key;
	variables: Variable[];
}

export enum Key {
	MottoBeX = "motto_be_x",
	MottoBreakYourXUponOurY = "motto_break_your_x_upon_our_y",
	MottoByGodAndX = "motto_by_god_and_x",
	MottoByTheXAndTheY = "motto_by_the_x_and_the_y",
	MottoByXAndY = "motto_by_x_and_y",
	MottoDareToBeX = "motto_dare_to_be_x",
	MottoForOurFoesXForOurFriendsY = "motto_for_our_foes_x_for_our_friends_y",
	MottoGodAndMyX = "motto_god_and_my_x",
	MottoGodXMe = "motto_god_x_me",
	MottoIX = "motto_i_x",
	MottoInMyXGodYMe = "motto_in_my_x_god_y_me",
	MottoMayGodXYKnight = "motto_may_god_x_y_knight",
	MottoMayXJoinMyCoa = "motto_may_x_join_my_coa",
	MottoNeverXAlwaysY = "motto_never_x_always_y",
	MottoNoX = "motto_no_x",
	MottoSingleNoun = "motto_single_noun",
	MottoSonOfX = "motto_son_of_x",
	MottoTheAncientXIsOurs = "motto_the_ancient_x_is_ours",
	MottoTheSpiritOfXGrantsUsY = "motto_the_spirit_of_x_grants_us_y",
	MottoTheXMindKnowsY = "motto_the_x_mind_knows_y",
	MottoThroughXMindY = "motto_through_x_mind_y",
	MottoUniquePool = "motto_unique_pool",
	MottoUntilOurXEngulfsTheWorld = "motto_until_our_x_engulfs_the_world",
	MottoVariousGodlyThings = "motto_various_godly_things",
	MottoWithTheXIYZ = "motto_with_the_x_i_y_z",
	MottoWithXISeekY = "motto_with_x_I_seek_y",
	MottoXAsY = "motto_x_as_y",
	MottoXFallsBeforeY = "motto_x_falls_before_y",
	MottoXIsY = "motto_x_is_y",
	MottoXMe = "motto_x_me",
	MottoXNotY = "motto_x_not_y",
	MottoXOverY = "motto_x_over_y",
	MottoXThroughGod = "motto_x_through_god",
	MottoXThroughY = "motto_x_through_y",
	MottoXUnderYKing = "motto_x_under_y_king",
	MottoXWithYInHand = "motto_x_with_y_in_hand",
	MottoXWithoutY = "motto_x_without_y",
	MottoXYAndZ = "motto_x_y_and_z",
	MottoXYZ = "motto_x_y_z",
}

export interface Variable {
	key: string;
	value: string;
}

export interface FactionManager {
	factions: { [key: string]: FactionClass | SexualityValue };
}

export interface FactionClass {
	type: FactionType;
	target: string;
	discontent?: string;
	power?: string;
	title_members?: TitleMember[];
	update_day: string;
	leader?: string;
	power_threshold?: string;
	members?: Member[];
	special_character?: string;
	special_title?: string;
	date?: string;
	war?: string;
	variables?: ArmyVariables;
}

export interface Member {
	character: string;
	faction: string;
	blocked?: string;
}

export interface TitleMember {
	county: string;
}

export enum FactionType {
	ClaimantFaction = "claimant_faction",
	IndependenceFaction = "independence_faction",
	LibertyFaction = "liberty_faction",
	PeasantFaction = "peasant_faction",
	PopulistFaction = "populist_faction",
}

export interface FleetManager {
	fleets: { [key: string]: FleetClass | SexualityValue };
}

export interface FleetClass {
	unit: string;
	embarking_army: string;
	atrtition_date: string;
}

export interface SaveGameRules {
	setting: string[];
}

export interface Holdings {
	constructions: string[];
}

export interface HolyOrders {
	holy_orders: { [key: string]: HolyOrder };
	religion_name: ReligionName;
	faith_name: FaithName;
}

export interface FaithName {
	"9": string[];
	size: string;
}

export interface HolyOrder {
	faith_name?: string;
	faith: string;
	title: string;
	titles: string[];
	regiments: string[];
	hired_by: string;
	army_regiments?: string[];
	religion_name?: string;
}

export interface ReligionName {
	"3": string[];
	"10": string[];
	"11": string[];
	size: string;
}

export interface ImportantActionManager {
	active: { [key: string]: FluffyActive | SexualityValue };
}

export interface FluffyActive {
	important_action_type: ImportantActionType;
	player: string;
	character: string;
	scope: ActiveScope;
	visible: FirstStart;
}

export enum ImportantActionType {
	ActionCanAskHofForMoney = "action_can_ask_hof_for_money",
	ActionCanAssignGuardian = "action_can_assign_guardian",
	ActionCanCallAlly = "action_can_call_ally",
	ActionCanCallDynastyMember = "action_can_call_dynasty_member",
	ActionCanCallHouseMember = "action_can_call_house_member",
	ActionCanChooseDynastyPerk = "action_can_choose_dynasty_perk",
	ActionCanCreateTitle = "action_can_create_title",
	ActionCanDemandPayment = "action_can_demand_payment",
	ActionCanLawfullyImprison = "action_can_lawfully_imprison",
	ActionCanProposeAllianceToFamilyMember = "action_can_propose_alliance_to_family_member",
	ActionCanRansom = "action_can_ransom",
	ActionCanUseHookToChangeContract = "action_can_use_hook_to_change_contract",
	ActionCanUsurpTitle = "action_can_usurp_title",
	ActionCanVassalize = "action_can_vassalize",
	ActionDangerousFactionTargetingMe = "action_dangerous_faction_targeting_me",
	ActionHasAnyDisplayCbOn = "action_has_any_display_cb_on",
	ActionHeirInLineOfSuccessionForTitle = "action_heir_in_line_of_succession_for_title",
	ActionInDebt = "action_in_debt",
	ActionInLineOfSuccessionForTitle = "action_in_line_of_succession_for_title",
	ActionLowControlInCounty = "action_low_control_in_county",
	ActionNotEndorsedByRealmPriest = "action_not_endorsed_by_realm_priest",
	ActionNotEnoughKnights = "action_not_enough_knights",
	ActionPowerfulVassalNotOnCouncil = "action_powerful_vassal_not_on_council",
	ActionTakeDecision = "action_take_decision",
	ActionTitleElection = "action_title_election",
	ActionTitleLostFromRealmOnForeignRulerSuccession = "action_title_lost_from_realm_on_foreign_ruler_succession",
	ActionTitleLostFromRealmOnSuccession = "action_title_lost_from_realm_on_succession",
	ActionTooManyHeldDuchies = "action_too_many_held_duchies",
	MercenaryContractExpiring = "mercenary_contract_expiring",
}

export interface ActiveScope {
	root: Special;
	seed: string;
	event_targets?: PurpleEventTargets;
}

export interface PurpleEventTargets {
	actor?: Special;
	recipient?: Special;
	landed_title?: Special;
	heir_title?: Special;
	decision_type?: ActivityOutcomeFlag;
	secondary_recipient?: Special;
	target?: Special;
}

export interface ActivityOutcomeFlag {
	type: SpecialType;
	flag: string;
}

export interface IronmanManager {
	date: string;
	save_game: string;
	save_storage: string;
}

export interface LandedTitles {
	dynamic_templates: DynamicTemplate[];
	landed_titles: { [key: string]: LandedTitle };
	index: string;
}

export interface DynamicTemplate {
	key: string;
	tier: Tier;
	dyn: FirstStart;
}

export enum Tier {
	Duchy = "duchy",
	Empire = "empire",
	Kingdom = "kingdom",
}

export interface LandedTitle {
	key: string;
	de_facto_liege?: string;
	holder?: string;
	name: string;
	adj?: string;
	article?: Article;
	date: string;
	history?: { [key: string]: Array<HistoryClass | string> | HistoryClass | string };
	capital?: string;
	history_government?: Government;
	coat_of_arms_id: string;
	laws?: LandedTitleLaw[];
	pre?: string;
	date_defeated_last_ai_raider?: string;
	heir?: string[];
	de_jure_vassals?: string[];
	claim?: string[];
	succession_election?: SuccessionElection;
	de_jure_liege?: string;
	drift_progress?: string[];
	capital_barony?: FirstStart;
	duchy_capital_barony?: FirstStart;
	theocratic_lease?: FirstStart;
	variables?: LandedTitleVariables;
	lessee?: string;
	localization_key?: string;
	drift_target?: string;
	reign_opinion_held_since?: string;
	color?: string[];
	landless?: FirstStart;
	destroy_if_invalid_heir?: FirstStart;
	no_automatic_claims?: FirstStart;
	definite_form?: FirstStart;
	special?: Special;
	destroy_on_succession?: FirstStart;
	delete_on_destroy?: FirstStart;
	can_be_named_after_dynasty?: FirstStart;
	renamed?: FirstStart;
}

export enum Article {
	The = "the ",
}

export interface HistoryClass {
	type: HistoryType;
	holder?: string;
}

export enum HistoryType {
	Abdication = "abdication",
	Conquest = "conquest",
	ConquestClaim = "conquest_claim",
	ConquestHolyWar = "conquest_holy_war",
	ConquestPopulist = "conquest_populist",
	Created = "created",
	Destroyed = "destroyed",
	FactionDemand = "faction_demand",
	Granted = "granted",
	Independency = "independency",
	LeasedOut = "leased_out",
	Returned = "returned",
	Revoked = "revoked",
	SwearFealty = "swear_fealty",
	Usurped = "usurped",
}

export enum LandedTitleLaw {
	EqualLaw = "equal_law",
	FeudalElectiveSuccessionLaw = "feudal_elective_succession_law",
	GaelicElectiveSuccessionLaw = "gaelic_elective_succession_law",
	MaleOnlyLaw = "male_only_law",
	MalePreferenceLaw = "male_preference_law",
	PrincelyElectiveSuccessionLaw = "princely_elective_succession_law",
	ScandinavianElectiveSuccessionLaw = "scandinavian_elective_succession_law",
	TemporalHeadOfFaithSuccessionLaw = "temporal_head_of_faith_succession_law",
}

export interface SuccessionElection {
	electors: string[];
	candidates: string[];
	nominations: Nomination[];
}

export interface Nomination {
	elector: string;
	candidate: string;
	strength: string;
}

export interface LandedTitleVariables {
	data: PurpleDatum[];
}

export interface Living {
	first_name: string;
	birth: string;
	female?: FirstStart;
	culture?: string;
	faith?: string;
	dynasty_house?: string;
	skill: string[];
	prowess_age?: string;
	dna?: string;
	weight?: Weight;
	traits?: string[];
	recessive_traits?: string[];
	family_data?: any[] | FluffyFamilyData;
	alive_data?: LivingAliveData;
	court_data?: any[] | CourtDataClass;
	landed_data?: LandedData;
	playable_data?: any[] | PlayableDataClass;
	was_playable?: FirstStart;
	sexuality?: SexualityValue;
	nickname?: Nickname;
	mass?: string;
	inactive_traits?: string[];
	dead_data?: LivingDeadData;
	regnal_name?: string;
}

export interface LivingAliveData {
	variables?: AliveDataVariables;
	gold?: string;
	income?: string;
	location?: string;
	modifier?: TimedModifierElement[] | TimedModifierElement;
	fertility: string;
	health: string;
	piety: DynastyPiety;
	prestige: DynastyPiety;
	focus?: Focus;
	secrets?: string[];
	targeting_secrets?: string[];
	targeting_schemes?: string[];
	claim?: Claim[];
	used_punishments?: { [key: string]: UsedPunishment };
	lifestyle_xp?: LifestyleXP;
	perk?: string[];
	weight_update?: string;
	kills?: string[];
	wars?: string[];
	stress?: string;
	schemes?: string[];
	pool_history?: string;
	activity?: string;
	pretender?: string[];
	heir?: string[];
	stories?: string[];
	prison_data?: PrisonData;
}

export interface Claim {
	title: string;
	pressed?: FirstStart;
}

export interface Focus {
	type: FocusType;
	date: string;
	changes: string;
	progress: string;
}

export enum FocusType {
	DiplomacyFamilyFocus = "diplomacy_family_focus",
	DiplomacyForeignAffairsFocus = "diplomacy_foreign_affairs_focus",
	DiplomacyMajestyFocus = "diplomacy_majesty_focus",
	EducationDiplomacy = "education_diplomacy",
	EducationIntrigue = "education_intrigue",
	EducationLearning = "education_learning",
	EducationMartial = "education_martial",
	EducationStewardship = "education_stewardship",
	IntrigueIntimidationFocus = "intrigue_intimidation_focus",
	IntrigueSkulduggeryFocus = "intrigue_skulduggery_focus",
	IntrigueTemptationFocus = "intrigue_temptation_focus",
	LearningMedicineFocus = "learning_medicine_focus",
	LearningScholarshipFocus = "learning_scholarship_focus",
	LearningTheologyFocus = "learning_theology_focus",
	MartialAuthorityFocus = "martial_authority_focus",
	MartialChivalryFocus = "martial_chivalry_focus",
	MartialStrategyFocus = "martial_strategy_focus",
	StewardshipDomainFocus = "stewardship_domain_focus",
	StewardshipDutyFocus = "stewardship_duty_focus",
	StewardshipWealthFocus = "stewardship_wealth_focus",
}

export interface LifestyleXP {
	learning_lifestyle?: string;
	martial_lifestyle?: string;
	diplomacy_lifestyle?: string;
	intrigue_lifestyle?: string;
	stewardship_lifestyle?: string;
}

export interface PrisonData {
	imprisoner: string;
	date: string;
	imprison_type_date: string;
	type: PrisonDataType;
}

export enum PrisonDataType {
	Dungeon = "dungeon",
	HouseArrest = "house_arrest",
}

export interface UsedPunishment {
	imprisonment_reason: FirstStart;
	revoke_title_reason?: FirstStart;
}

export interface AliveDataVariables {
	data?: TentacledDatum[];
	list?: FluffyList[];
}

export interface TentacledDatum {
	flag: string;
	data: any[] | DataElement;
	tick?: string;
}

export interface DataElement {
	type: SpecialType;
	flag?: string;
	identity?: string;
}

export interface FluffyList {
	name: FluffyName;
	item: DataElement[] | DataElement;
}

export enum FluffyName {
	ClaimantFactions = "claimant_factions",
	ExtraStressThresholdEventOption = "extra_stress_threshold_event_option",
	HadRelationUpgrade0001 = "had_relation_upgrade_0001",
	LoverObjectOfImportance = "lover_object_of_importance",
	LoverObjectOfImportance2 = "lover_object_of_importance_2",
	PotentialFatherVariableList = "potential_father_variable_list",
}

export interface CourtDataClass {
	council_task?: string;
	knight?: FirstStart;
	employer?: string;
	wants_to_leave_court?: FirstStart;
	leave_court_date?: string;
	host?: string;
	special_council_tasks?: string[];
	army?: string;
	regiment?: string;
}

export interface LivingDeadData {
	date: string;
	reason: Reason;
	liege: string;
	liege_title: string;
	domain?: string[];
	kills?: string[];
	government?: Government;
}

export interface FluffyFamilyData {
	spouse?: string[] | string;
	former_spouses?: string[];
	child?: string[];
	primary_spouse?: string;
	real_father?: string;
	former_concubinists?: string[];
	concubine?: string[] | string;
	former_concubines?: string[];
	betrothed?: string;
	concubinist?: string;
}

export interface LandedData {
	domain: string[];
	vassal_contracts?: string[];
	last_war_finish_date?: string;
	became_ruler_date: string;
	laws: LandedDataLaw[];
	strength?: string;
	balance?: string;
	dread?: string;
	succession?: string[];
	domain_limit: string;
	vassal_limit?: string;
	vassals_towards_limit?: string;
	government: Government;
	realm_capital?: string;
	ai_allowed_to_marry?: FirstStart;
	council?: string[];
	at_peace_penalty?: string;
	diplo_centers?: string[];
	decision_cooldowns?: DecisionCooldowns;
	strength_for_liege?: string;
	liege_tax?: string;
	is_powerful_vassal?: FirstStart;
	vassal_power_value?: string;
	election_titles?: string[];
	interaction_cooldowns?: InteractionCooldowns;
	owned_activities?: string[];
	tyranny?: string;
	known_schemes?: string[];
	absolute_control?: FirstStart;
	realm_capital_moved?: FirstStart;
	last_raid?: string;
	units?: string[];
	wars?: string[];
	war_opinion?: string;
	event_troops?: EventTroop[];
	activity_invitations?: string[];
}

export interface DecisionCooldowns {
	go_on_pilgrimage_decision?: string;
	start_hunt_decision?: string;
	hold_mystical_communion_decision?: string;
	host_feast_decision?: string;
	meditate_in_seclusion_decision?: string;
	go_on_hajj_decision?: string;
	stress_loss_flagellant_decision?: string;
	commission_epic_decision?: string;
	borrow_from_holy_order_decision?: string;
	commit_suicide_decision?: string;
	sale_of_titles_decision?: string;
	extort_subjects_decision?: string;
	stress_loss_hashishiyah_decision?: string;
	stress_loss_athletic_decision?: string;
	stress_loss_rakish_decision?: string;
	stress_loss_drunkard_decision?: string;
	stress_loss_reclusive_decision?: string;
	stress_loss_profligate_decision?: string;
	stress_loss_irritable_decision?: string;
	stress_loss_journaller_decision?: string;
	host_witch_ritual_decision?: string;
	stress_loss_improvident_decision?: string;
	invite_claimants_decision?: string;
	hire_physician_decision?: string;
	stress_loss_confider_decision?: string;
}

export interface EventTroop {
	name: string;
	regiments: string[];
}

export interface InteractionCooldowns {
	host_honored_guest_interaction?: string;
	seek_indulgences_interaction?: string;
}

export enum LandedDataLaw {
	BishopTheocraticSuccessionLaw = "bishop_theocratic_succession_law",
	CitySuccessionLaw = "city_succession_law",
	ConfederatePartitionSuccessionLaw = "confederate_partition_succession_law",
	CrownAuthority0 = "crown_authority_0",
	CrownAuthority1 = "crown_authority_1",
	CrownAuthority2 = "crown_authority_2",
	CrownAuthority3 = "crown_authority_3",
	EqualLaw = "equal_law",
	HighPartitionSuccessionLaw = "high_partition_succession_law",
	HolyOrderSuccessionLaw = "holy_order_succession_law",
	MaleOnlyLaw = "male_only_law",
	MalePreferenceLaw = "male_preference_law",
	MercenaryCompanySuccessionLaw = "mercenary_company_succession_law",
	PartitionSuccessionLaw = "partition_succession_law",
	SameFaithTheocraticSuccessionLaw = "same_faith_theocratic_succession_law",
	SingleHeirDynastyHouse = "single_heir_dynasty_house",
	SingleHeirSuccessionLaw = "single_heir_succession_law",
	TribalAuthority0 = "tribal_authority_0",
	TribalAuthority1 = "tribal_authority_1",
	TribalAuthority3 = "tribal_authority_3",
}

export interface PlayableDataClass {
	knights: string[];
	was_player?: FirstStart;
}

export interface Weight {
	base?: string;
	current?: string;
	target?: string;
}

export interface MercenaryCompanyManager {
	mercenary_companies: { [key: string]: MercenaryCompanyClass | SexualityValue };
}

export interface MercenaryCompanyClass {
	culture_group_name?: string;
	culture: string;
	county: string;
	size: string;
	title: string;
	regiments: string[];
	hired_by?: string;
	hired_until?: string;
	culture_name?: string;
}

export interface MetaData {
	save_game_version: string;
	version: string;
	portraits_version: string;
	meta_date: string;
	meta_player_name: string;
	meta_title_name: string;
	meta_coat_of_arms: MetaCoatOfArms;
	meta_player_tier: string;
	meta_house_name: string;
	meta_house_coat_of_arms: MetaCoatOfArms;
	meta_dynasty_frame: string;
	meta_main_portrait: MetaPortrait;
	meta_heir_portrait: MetaPortrait;
	meta_secondary_portrait: MetaPortrait;
	meta_front_end_background: string;
	meta_government: Government;
	meta_real_date: string;
	meta_number_of_players: string;
	game_rules: MetaDataGameRules;
	can_get_achievements: FirstStart;
	ironman: FirstStart;
}

export interface MetaDataGameRules {
	settings: string[];
}

export interface MetaCoatOfArms {
	pattern: Pattern;
	color1: Color;
	color2: Color;
	colored_emblem?: MetaCoatOfArmsColoredEmblem[];
	sub?: MetaCoatOfArmsSub[];
}

export interface MetaCoatOfArmsColoredEmblem {
	color1: Color;
	color2: Color;
	texture: string;
	instance?: PurpleInstance[] | FluffyInstance;
}

export interface FluffyInstance {
	position: string[];
}

export interface MetaCoatOfArmsSub {
	instance: SubInstance;
	pattern: Pattern;
	color1: Color;
	color2: Color;
	colored_emblem: MetaCoatOfArmsColoredEmblem[] | MetaCoatOfArmsColoredEmblem;
}

export interface MetaPortrait {
	type: string;
	id: string;
	age: string;
	genes: { [key: string]: string[] };
	entity: string[];
}

export interface Opinions {
	active_opinions: ActiveOpinion[];
}

export interface ActiveOpinion {
	owner: string;
	target: string;
	temporary_opinion?: TemporaryOpinionElement[] | TemporaryOpinionElement;
	scripted_relations?: { [key: string]: string[] };
	delete?: FirstStart;
}

export interface TemporaryOpinionElement {
	modifier: string;
	start_date: string;
	expiration_date: string;
	converging?: Converging;
	value: string;
	revoke_title_reason?: string;
	used_punishments?: UsedPunishments;
}

export interface Converging {
	days?: string;
	opinion: string;
	total_days?: string;
}

export interface UsedPunishments {
	imprisonment_reason: FirstStart;
}

export interface PendingCharacterInteractions {
	data: { [key: string]: DatumDatum | SexualityValue };
	player: Player;
}

export interface DatumDatum {
	interaction_data: InteractionData;
	days_in_queue: string;
	ai_reply_delay: string;
}

export interface InteractionData {
	character_interaction: string;
	scope: InteractionDataScope;
	original_scope: OriginalScope;
	actor: string;
	recipient: string;
	secondary_recipient: string;
	option?: string;
	secondary_actor?: string;
	arrange_marriage_interaction?: any[];
}

export interface OriginalScope {
	root: any[];
	seed: string;
}

export interface InteractionDataScope {
	root: any[];
	seed: string;
	event_targets: FluffyEventTargets;
}

export interface FluffyEventTargets {
	actor: Special;
	recipient: Special;
	secondary_actor: Special;
	secondary_recipient: Special;
	demand_conversion?: IsChildOfConcubine;
	renounce_claims?: Special;
	banish?: IsChildOfConcubine;
	gain_hook?: IsChildOfConcubine;
	take_vows?: IsChildOfConcubine;
	recruit?: IsChildOfConcubine;
	gold?: Special;
	current_gold?: IsChildOfConcubine;
	favor?: Special;
	invalid?: IsChildOfConcubine;
	hook?: IsChildOfConcubine;
	matrilineal?: IsChildOfConcubine;
	marriage_hook?: IsChildOfConcubine;
}

export interface IsChildOfConcubine {
	type: SpecialType;
}

export interface Player {
	owner: string;
	rejected: Rejected;
}

export interface Rejected {
	data: RejectedData;
	end_date: string;
}

export interface RejectedData {
	character_interaction: string;
	scope: DataScope;
	original_scope: OriginalScope;
	actor: string;
	recipient: string;
	secondary_recipient: string;
	option: string;
}

export interface DataScope {
	root: any[];
	seed: string;
	event_targets: TentacledEventTargets;
}

export interface TentacledEventTargets {
	actor: Special;
	recipient: Special;
	secondary_actor: Special;
	secondary_recipient: Special;
	gold: Special;
	current_gold: IsChildOfConcubine;
	favor: IsChildOfConcubine;
	invalid?: IsChildOfConcubine;
	hook?: IsChildOfConcubine;
}

export interface PlayedCharacter {
	name: string;
	character: string;
	player: string;
	important_decisions: ImportantDecisions;
	legacy: Legacy[];
	rally_points: RallyPoint[];
}

export interface ImportantDecisions {
	create_holy_order_decision: FirstStart;
	restore_pope_in_rome_decision: FirstStart;
}

export interface Legacy {
	character: string;
	date: string;
	score?: string;
	prestige?: string;
	piety?: string;
	dynasty?: string;
	lifestyle?: string;
	wars: string[];
	perk?: string[] | string;
	cultural_head?: FirstStart;
	dread?: string;
}

export interface RallyPoint {
	province: string;
	color: string;
}

export interface Province {
	holding: Holding;
	fort_level?: string;
	timed_modifier?: TimedModifierElement[] | TimedModifierElement;
	occupant?: string;
	variables?: ArmyVariables;
}

export interface Holding {
	buildings?: Array<any[] | DuchyCapitalBuildingClass>;
	levy?: string;
	garrison?: string;
	income: string;
	type?: HoldingType;
	duchy_capital_building?: DuchyCapitalBuildingClass;
	constructions?: Constructions;
	special_building_type?: string;
	domain_limit_grace_period?: FirstStart;
	special_building?: DuchyCapitalBuildingClass;
}

export interface DuchyCapitalBuildingClass {
	type: string;
	disabled?: FirstStart;
}

export interface Constructions {
	building: string;
	index: string;
	start_time: string;
	days: string;
	cost: Cost;
}

export enum HoldingType {
	CastleHolding = "castle_holding",
	ChurchHolding = "church_holding",
	CityHolding = "city_holding",
	TribalHolding = "tribal_holding",
}

export interface RAID {
	raid: { [key: string]: SexualityValue };
}

export interface Relations {
	active_relations: ActiveRelation[];
}

export interface ActiveRelation {
	first: string;
	second: string;
	alliances?: Alliance[];
	cooldown_against_recipient_1?: CooldownAgainstRecipient;
	active_hook_0?: ActiveHook;
	active_hook_1?: ActiveHook;
	cooldown_against_recipient_0?: CooldownAgainstRecipient;
	war?: string;
	scheme_cooldowns_1?: SchemeCooldowns;
	scheme_cooldowns_0?: SchemeCooldowns;
	truce_0?: Truce;
	truce_1?: Truce;
	matrilineal?: FirstStart;
	raid_immunity_0?: string;
	raid_0?: string;
	raid_1?: string;
}

export interface ActiveHook {
	type: ActiveHook0_Type;
	gold: string;
	expiration_date: string;
	secret?: string;
	cooldown?: string;
}

export enum ActiveHook0_Type {
	FabricationHook = "fabrication_hook",
	FavorHook = "favor_hook",
	HouseHeadHook = "house_head_hook",
	IndebtedHook = "indebted_hook",
	LifeThreatHook = "life_threat_hook",
	LoyaltyHook = "loyalty_hook",
	ManipulationHook = "manipulation_hook",
	StrongBlackmailHook = "strong_blackmail_hook",
	ThreatHook = "threat_hook",
	WeakBlackmailHook = "weak_blackmail_hook",
}

export interface Alliance {
	allied_through_0: string;
	allied_through_1: string;
}

export interface CooldownAgainstRecipient {
	take_vows_interaction?: string;
	hof_ask_for_gold_interaction?: string;
	demand_conversion_vassal_ruler_interaction?: string;
	torture_interaction?: string;
}

export interface SchemeCooldowns {
	befriend?: string;
	murder?: string;
	courting?: string;
	fabricate_hook?: string;
	elope?: string;
}

export interface Truce {
	date: string;
	name: string;
	result?: Result;
}

export enum Result {
	Defeat = "defeat",
	Victory = "victory",
	WhitePeace = "white_peace",
}

export interface SaveFileReligion {
	religions: { [key: string]: ReligionValue };
	faiths: { [key: string]: Faith };
	great_holy_wars: { [key: string]: SexualityValue };
	holy_sites: { [key: string]: HolySite };
}

export interface Faith {
	template: string;
	tag: string;
	color: string[];
	icon: string;
	texticon: string;
	graphical_faith: FaithGraphicalFaith;
	piety_icon_group: string;
	doctrine_background_icon: string;
	fervor: string;
	religious_head: string;
	doctrine: string[];
	religion: string;
	holy_sites: string[];
	variables?: LandedTitleVariables;
	name?: string;
	adjective?: string;
	adherent?: string;
	adherent_plural?: string;
	desc?: string;
	founder?: string;
}

export enum FaithGraphicalFaith {
	CatholicGfx = "catholic_gfx",
	Empty = "",
}

export interface HolySite {
	holy_site_template: string;
	is_active?: FirstStart;
}

export interface ReligionValue {
	template: string;
	tag: string;
	graphical_faith: ReligionGraphicalFaith;
	piety_icon_group: PietyIconGroup;
	doctrine_background_icon: DoctrineBackgroundIcon;
	family: Family;
	faiths: string[];
	variables?: ArmyVariables;
}

export enum DoctrineBackgroundIcon {
	CoreTenetBannerChristianDds = "core_tenet_banner_christian.dds",
	CoreTenetBannerIslamDds = "core_tenet_banner_islam.dds",
	Empty = "",
}

export enum Family {
	RFAbrahamic = "rf_abrahamic",
	RFEastern = "rf_eastern",
	RFPagan = "rf_pagan",
}

export enum ReligionGraphicalFaith {
	DharmicGfx = "dharmic_gfx",
	Empty = "",
	IslamicGfx = "islamic_gfx",
	JudaismGfx = "judaism_gfx",
	PaganGfx = "pagan_gfx",
}

export enum PietyIconGroup {
	Christian = "christian",
	Empty = "",
	Islam = "islam",
}

export interface Schemes {
	active: { [key: string]: TentacledActive | SexualityValue };
}

export interface TentacledActive {
	type: TentacledType;
	owner: string;
	target: string;
	progress: string;
	variables?: PurpleVariables;
	update_days: string;
	scheme_exposed: FirstStart;
	owner_influence: string;
	secrecy: string;
	chance: string;
	modifier: any[] | PurpleModifier;
	scheme_freeze_days: string;
	date: string;
	owner_agent?: OwnerAgentElement[] | PurpleOwnerAgent;
	forbidden_character?: string[];
	timed_modifier?: TimedModifierClass[] | TimedModifierClass;
}

export interface PurpleModifier {
	scheme_secrecy?: string;
	scheme_success_chance?: string;
	scheme_power?: string;
}

export interface OwnerAgentElement {
	character: string;
	influence: string;
}

export interface PurpleOwnerAgent {
	character: string;
	influence: string;
	exposed?: FirstStart;
	force?: string;
}

export interface TimedModifierClass {
	date: DateEnum;
	modifier: string;
}

export enum DateEnum {
	The1398516 = "1398.5.16",
	The139858 = "1398.5.8",
	The999911 = "9999.1.1",
}

export enum TentacledType {
	Befriend = "befriend",
	ClaimThrone = "claim_throne",
	ConvertToWitchcraft = "convert_to_witchcraft",
	Courting = "courting",
	FabricateHook = "fabricate_hook",
	Murder = "murder",
	Seduce = "seduce",
	Sway = "sway",
}

export interface PurpleVariables {
	data: StickyDatum[];
}

export interface StickyDatum {
	flag: string;
	data: DataElement;
	tick?: string;
}

export interface Secrets {
	secrets: { [key: string]: SecretClass | SexualityValue };
	known_secrets: KnownSecret[];
}

export interface KnownSecret {
	secret: string;
	owner: string;
}

export interface SecretClass {
	type: SecretType;
	target: any[] | Special;
	owner: string;
	participants: string[];
	relation_type?: RelationType;
	spent_by?: string[];
	cannot_expose?: string[];
	exposed?: FirstStart;
	delete?: FirstStart;
	variables?: ArmyVariables;
}

export enum RelationType {
	Lover = "lover",
	Soulmate = "soulmate",
}

export enum SecretType {
	SecretCannibal = "secret_cannibal",
	SecretDeviant = "secret_deviant",
	SecretDisputedHeritage = "secret_disputed_heritage",
	SecretHomosexual = "secret_homosexual",
	SecretIncest = "secret_incest",
	SecretLover = "secret_lover",
	SecretMurder = "secret_murder",
	SecretMurderAttempt = "secret_murder_attempt",
	SecretNonBeliever = "secret_non_believer",
	SecretUnmarriedIllegitimateChild = "secret_unmarried_illegitimate_child",
	SecretWitch = "secret_witch",
}

export interface Sieges {
	sieges: { [key: string]: SiegeClass | SexualityValue };
}

export interface SiegeClass {
	province: string;
	morale: string;
	siege_phase_time: string;
	action_history_head: string;
	action_history: ActionHistory[];
	starvation?: string;
	stalemate?: string;
	random: string;
	start_date: string;
	owner: string;
	desertion?: string;
	disease?: string;
	breach?: string;
}

export enum ActionHistory {
	Breach = "breach",
	Desertion = "desertion",
	Disease = "disease",
	None = "none",
	Stalemate = "stalemate",
	Starvation = "starvation",
}

export interface Stories {
	active: { [key: string]: StickyActive | SexualityValue };
	next: string;
}

export interface StickyActive {
	story_cycle: StoryCycle;
	owner: string;
	dates: string[];
	seed: string;
	variables: FluffyVariables;
}

export enum StoryCycle {
	CommissionEpicStory = "commission_epic_story",
	StoryCycleMartialLifestyleWarhorse = "story_cycle_martial_lifestyle_warhorse",
	StoryCyclePetDog = "story_cycle_pet_dog",
	StoryPeasantAffair = "story_peasant_affair",
}

export interface FluffyVariables {
	data: StickyDatum[];
}

export interface TriggeredEvent {
	on_action?: OnAction;
	scope: TriggeredEventScope;
	date: string;
	event?: EventEnum;
	priority?: string;
}

export enum EventEnum {
	Adultery0001 = "adultery.0001",
	Adultery1003 = "adultery.1003",
	Adultery1009 = "adultery.1009",
	Adultery1101 = "adultery.1101",
	Birth3001 = "birth.3001",
	Birth3021 = "birth.3021",
	Birth3034 = "birth.3034",
	ChildPersonality4000 = "child_personality.4000",
	ChildPersonality4012 = "child_personality.4012",
	ChildPersonality4013 = "child_personality.4013",
	ChildPersonality4014 = "child_personality.4014",
	ChildPersonality4015 = "child_personality.4015",
	ChildhoodEducation1021 = "childhood_education.1021",
	ChildhoodEducation1031 = "childhood_education.1031",
	CommissionEpic0002 = "commission_epic.0002",
	CouncillorSpouseBackground0001 = "councillor_spouse_background.0001",
	DeathManagement9999 = "death_management.9999",
	DiplomacyForeign3003 = "diplomacy_foreign.3003",
	DiplomacyForeign3007 = "diplomacy_foreign.3007",
	Feast1501 = "feast.1501",
	FeastDefault9000 = "feast_default.9000",
	Health0201 = "health.0201",
	Health1010 = "health.1010",
	Health1013 = "health.1013",
	Health1014 = "health.1014",
	Health1101 = "health.1101",
	Health1102 = "health.1102",
	Health1103 = "health.1103",
	Health1105 = "health.1105",
	Health1106 = "health.1106",
	Health1107 = "health.1107",
	Health1109 = "health.1109",
	Health1110 = "health.1110",
	Health1200 = "health.1200",
	Health1201 = "health.1201",
	Health1202 = "health.1202",
	Health3100 = "health.3100",
	Health3102 = "health.3102",
	Health4001 = "health.4001",
	Health4999 = "health.4999",
	Health6100 = "health.6100",
	Hunt1003 = "hunt.1003",
	Hunt9001 = "hunt.9001",
	IntrigueScheming1202 = "intrigue_scheming.1202",
	IntrigueTemptationSpecial1007 = "intrigue_temptation_special.1007",
	MartialAuthority2051 = "martial_authority.2051",
	Pregnancy2101 = "pregnancy.2101",
	RealmMaintenance0004 = "realm_maintenance.0004",
	SeduceOngoing1411 = "seduce_ongoing.1411",
	StewardshipDomainSpecial1443 = "stewardship_domain_special.1443",
	StewardshipDomainSpecial8013 = "stewardship_domain_special.8013",
	StewardshipDomainSpecial8031 = "stewardship_domain_special.8031",
	StewardshipDuty1063 = "stewardship_duty.1063",
	StressThreshold0001 = "stress_threshold.0001",
	StressThreshold0005 = "stress_threshold.0005",
	TraitSpecific1001 = "trait_specific.1001",
	TraitSpecific1002 = "trait_specific.1002",
	TraitSpecific1010 = "trait_specific.1010",
	TraitSpecific2001 = "trait_specific.2001",
	TraitSpecific2002 = "trait_specific.2002",
	TraitSpecific3999 = "trait_specific.3999",
	Vassal1020 = "vassal.1020",
	Vassal1120 = "vassal.1120",
	Vassal2120 = "vassal.2120",
	Vassal2911 = "vassal.2911",
	WarEvent3001 = "war_event.3001",
}

export enum OnAction {
	ChildPersonalityGain = "child_personality_gain",
	ClaimThroneOngoing = "claim_throne_ongoing",
	FabricateHookOngoing = "fabricate_hook_ongoing",
	FaithFervorEventsPulse = "faith_fervor_events_pulse",
	FeastDefaultEventSelection = "feast_default_event_selection",
	FeastDefaultHostEventSelection = "feast_default_host_event_selection",
	FeastMainEventSelection = "feast_main_event_selection",
	HajjJourneyEvents = "hajj_journey_events",
	HuntRandomPulse = "hunt_random_pulse",
	LearnCommanderTraitTest = "learn_commander_trait_test",
	OrganizeTheLeviesMarshalTaskSideEffects = "organize_the_levies_marshal_task_side_effects",
	PilgrimageDestinationEvents = "pilgrimage_destination_events",
	PilgrimageJourneyEvents = "pilgrimage_journey_events",
	PilgrimageReturnEvents = "pilgrimage_return_events",
	QuarterlyPrisonMaintenancePulse = "quarterly_prison_maintenance_pulse",
	SpouseCouncillorDefaultOnAction = "spouse_councillor_default_on_action",
	TaskCollectTaxesSideEffects = "task_collect_taxes_side_effects",
	TaskConvertSideEffects = "task_convert_side_effects",
	TaskDevelopCountySideEffects = "task_develop_county_side_effects",
	TaskDisruptSchemesSideEffects = "task_disrupt_schemes_side_effects",
	TaskDomesticAffairsSideEffects = "task_domestic_affairs_side_effects",
	TaskFabricateClaimSideEffects = "task_fabricate_claim_side_effects",
	TaskForeignAffairsSideEffects = "task_foreign_affairs_side_effects",
	TaskIncreaseControlSideEffects = "task_increase_control_side_effects",
	TaskIntegrateTitleSideEffects = "task_integrate_title_side_effects",
	TaskPromoteCultureSideEffects = "task_promote_culture_side_effects",
	TaskReligiousRelationsSideEffects = "task_religious_relations_side_effects",
	TrainCommandersMarshalTaskSideEffects = "train_commanders_marshal_task_side_effects",
	WoundedRecoveryPulse = "wounded_recovery_pulse",
}

export interface TriggeredEventScope {
	root: Special;
	seed: string;
	event_targets?: StickyEventTargets;
	variables?: LandedTitleVariables;
	list?: ScopeList[];
}

export interface StickyEventTargets {
	activity?: Special;
	secret_recipient?: Special;
	secret_owner?: Special;
	secret_to_reveal?: Special;
	councillor?: Special;
	councillor_liege?: Special;
	province?: Special;
	county?: Special;
	impressed_guest?: Special;
	other_guest?: Special;
	event_counter?: Special;
	halfway_distance?: Special;
	dummy_peasant_gender?: Special;
	title?: Special;
	mother?: Special;
	father?: Special;
	real_father?: Special;
	is_child_of_concubine?: IsChildOfConcubine;
	matrilineal?: Special;
	spouse?: Special;
	adultery_spouse?: Special;
	host?: Special;
	quarter?: Special;
	ruler?: Special;
	neighboring_ruler?: Special;
	war?: Special;
	guest_to_talk_to?: Special;
	sadistic_host?: Special;
	guest?: Special;
	other_glutton?: Special;
	scheme?: Special;
	owner?: Special;
	target?: Special;
	real_value?: Special;
	court_location?: Special;
	gift?: ActivityOutcomeFlag;
	writer?: Special;
	composer?: Special;
	sick_character?: Special;
	disease_type?: ActivityOutcomeFlag;
	infecting_partner?: Special;
	corpse_rival?: Special;
	capital?: Special;
	rival?: Special;
	rival_capital?: Special;
	peasant?: Special;
	your_courtier_to_infect?: Special;
	their_courtier_to_infect?: Special;
	disease_spreader?: Special;
	contagion_court_owner?: Special;
	spymaster?: Special;
	favor_character?: Special;
	sadist?: Special;
	low_skill_option?: Special;
	stress_character?: Special;
	scoped_ruler?: Special;
	deceased_character?: Special;
	confidant?: Special;
	destination_title?: Special;
	friend?: Special;
	land_owner?: Special;
	from_stress_loss_decision?: Special;
	steward?: Special;
	chancellor?: Special;
	had_sex_with_effect_partner?: Special;
	combat_side?: CombatSide;
	physician?: Special;
	treatment_picker?: Special;
	troubled_guest?: Special;
	glutton?: Special;
	child?: Special;
	is_bastard?: Special;
	character_to_yell_at?: Special;
	drunk?: Special;
	naked_preacher?: Special;
	hunt_guest?: Special;
	hunting_courtier_1?: Special;
	start_location?: Special;
	health_court_owner?: Special;
	corruption_holder?: Special;
	primary_seducee?: Special;
	secondary_seducee?: Special;
	highest_intrigue_seducee?: Special;
	increase_blademaster?: Special;
	prisoner?: Special;
	lost_friend?: Special;
	fornicator_check?: Special;
	std_partner?: Special;
	other_partner?: Special;
	guest_lover?: Special;
	guest_lovers_friend?: Special;
	first_topic_to_write_about?: ActivityOutcomeFlag;
	option_chosen?: ActivityOutcomeFlag;
	guardian?: Special;
	dummy_clergy_gender?: Special;
	zealous_priest?: Special;
	activity_place?: Special;
	county_owner?: Special;
	teacher?: Special;
	scheme_successful?: Special;
	discovery_chance?: Special;
	event_location?: Special;
	beggar?: Special;
	defender?: Special;
	scheme_location?: Special;
	background_terrain_scope?: Special;
	hunter?: Special;
	dummy_gender?: Special;
	target_host?: Special;
	become_soulmate?: Special;
	child_preacher?: Special;
	neglected_spouse?: Special;
	rival_main?: Special;
	rival_spouse?: Special;
	rival_scale?: Special;
	breakin_stager?: Special;
	breakin_helper?: Special;
	breakin_bystander?: Special;
	court_physician?: Special;
	scoped_faith?: Special;
	scoped_helper?: Special;
	wounded_vassal?: Special;
	helper?: Special;
	soldier?: Special;
	jailer?: Special;
	love_letter_target?: Special;
	letterAuthor?: Special;
	project_type?: ActivityOutcomeFlag;
	dummy_servant_gender?: Special;
	client_1?: Special;
	dummy_scholar_gender?: Special;
	client_2?: Special;
	client_3?: Special;
	client?: Special;
	party_vassal?: Special;
	confessed_secret?: Special;
	scoped_capital?: Special;
	partner?: Special;
	capital_province?: Special;
	married_seducer?: Special;
	seducer_spouse?: Special;
	message_receiver?: Special;
	partner_spouse?: Special;
	second_confessed_secret?: Special;
	root_character?: Special;
	barony?: Special;
	collector?: Special;
	new_physician?: Special;
	infection?: Special;
	poison?: Special;
	friendly_person?: Special;
	scheme_discovered?: Special;
	victim?: Special;
	fellow_prisoner?: Special;
	scoped_prisoner?: Special;
	wants_love?: Special;
	preference_flag?: ActivityOutcomeFlag;
	preference_check_flag?: ActivityOutcomeFlag;
	preference_check_outcome_flag?: ActivityOutcomeFlag;
	target_list_member_a?: Special;
	target_list_member_b?: Special;
	target_list_member_c?: Special;
	chosen_match?: Special;
	match_status_flag?: ActivityOutcomeFlag;
	exclude_option_flag?: ActivityOutcomeFlag;
	activity_skill_flag?: ActivityOutcomeFlag;
	activity_outcome_flag?: ActivityOutcomeFlag;
	right_hand_person?: Special;
	marshal?: Special;
	enemy_knight?: Special;
	vassal?: Special;
	background_wilderness_scope?: Special;
	grand_rite_ceremony_outcome?: ActivityOutcomeFlag;
	sex_partner?: Special;
	left_portrait?: Special;
	court_of_residence?: Special;
	religious_vassal?: Special;
	tempted_guest?: Special;
	claim_vassal?: Special;
	claim_to_propose?: Special;
	unlucky_sod?: Special;
	religious_liege?: Special;
}

export interface CombatSide {
	type: CombatSideType;
	identity: string;
	data?: string;
}

export enum CombatSideType {
	Combsi = "combsi",
}

export interface ScopeList {
	name: string;
	item: Special[] | Special;
}

export interface UnitClass {
	type: UnitType;
	location: string;
	prev?: string;
	path?: string[];
	total_cost?: string;
	movement_progress?: string;
	owner: string;
	army?: string;
	arrival_date: string;
	banned_colors: string;
	ai_unit_data: any[];
	fleet?: string;
	retreating?: Retreating;
}

export enum Retreating {
	RetreatShattered = "retreat_shattered",
}

export enum UnitType {
	Army = "army",
	Fleet = "fleet",
}

export interface SaveVariables {
	data: FluffyDatum[];
	list: TentacledList[];
}

export interface TentacledList {
	name: string;
	item: ActivityOutcomeFlag[] | ActivityOutcomeFlag;
}

export interface VassalContracts {
	active: { [key: string]: IndigoActive | SexualityValue };
}

export interface IndigoActive {
	vassal: string;
	liege: string;
	date: string;
	levels: string[];
	opinion_of_liege?: string;
	allegiance?: string;
	blocked?: FirstStart;
	war_with_liege?: FirstStart;
}

export interface Wars {
	active_wars: { [key: string]: ActiveWarClass | SexualityValue };
	names: { [key: string]: string[] | string };
}

export interface ActiveWarClass {
	attacker: ActiveWarAttacker;
	defender: ActiveWarAttacker;
	start_date: string;
	casus_belli: CasusBelli;
	battle_results?: BattleResult[];
	called_to_war?: string[];
	name: string;
	variables?: ArmyVariables;
	reached_max_score_date?: string;
}

export interface ActiveWarAttacker {
	participants: Participant[];
	ticking_war_score?: string;
	controls_all?: FirstStart;
}

export interface Participant {
	character: string;
	last_action: string;
	contribution: string[];
}

export interface BattleResult {
	attacker: BattleResultAttacker;
	defender: BattleResultAttacker;
	province: string;
	war_score: string;
	attacker_won: FirstStart;
	attacker_initiated: FirstStart;
}

export interface BattleResultAttacker {
	commander: string;
	owner: string;
	size: string;
}

export interface CasusBelli {
	type: string;
	scope: CasusBelliScope;
	targeted_titles?: string[];
	attacker: string;
	defender: string;
	claimant: string;
}

export interface CasusBelliScope {
	root: Special;
	seed: string;
}
