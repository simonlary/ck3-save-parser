import tokenize from "../src/tokenizer";

function tokenizerTest(name: string, input: string, output: string[]) {
	it(name, () => {
		expect(tokenize(input)).toEqual(output);
	});
}

describe("Tokenizer", () => {
	tokenizerTest("tokenizes an empty string.", "", []);

	tokenizerTest("tokenizes only whitespaces.", " 	", []);

	tokenizerTest("tokenizes a single string.", "test", ["test"]);

	tokenizerTest("tokenizes a simple key/value.", "key=value", ["key", "=", "value"]);

	tokenizerTest("tokenizes a date.", "date=1398.3.1", ["date", "=", "1398.3.1"]);

	tokenizerTest("tokenizes a quoted string with spaces.", `name="Emperor Gaston the Scholar"`, [
		"name",
		"=",
		"Emperor Gaston the Scholar",
	]);

	tokenizerTest("tokenizes an empty quoted string.", `name=""`, ["name", "=", ""]);

	tokenizerTest("tokenizes a quoted string with a missing end quote.", `name="Emperor Gaston the Scholar`, [
		"name",
		"=",
		"Emperor Gaston the Scholar",
	]);

	tokenizerTest(
		"tokenizes an object.",
		`object={
			first="value"
			second=1.2.3
		}`,
		["object", "=", "{", "first", "=", "value", "second", "=", "1.2.3", "}"]
	);

	tokenizerTest("tokenizes a value array.", `array={ 123.456 456.789 }`, [
		"array",
		"=",
		"{",
		"123.456",
		"456.789",
		"}",
	]);
});
