import { parseContent, parseFile } from "../src";
import * as fs from "fs/promises";
import path from "path";

// eslint-disable-next-line @typescript-eslint/no-explicit-any
function parseContentTest(name: string, input: string, output: any) {
	it(name, () => {
		expect(parseContent(input)).toEqual(output);
	});
}

describe("parseContent()", () => {
	parseContentTest("parses an empty string", "", {});
	parseContentTest(
		"parses simple save file",
		`meta_data={
			save_game_version=3
			version="1.2.1"
			meta_coat_of_arms={
				pattern="pattern_solid.dds"
				color1=red
				color2=yellow
			}
		}
		ironman_manager={
			date=1066.9.15
			save_game=""
			save_storage=local
		}
		date=1398.3.1
		bookmark_date=1066.9.15`,
		{
			meta_data: {
				save_game_version: "3",
				version: "1.2.1",
				meta_coat_of_arms: {
					pattern: "pattern_solid.dds",
					color1: "red",
					color2: "yellow",
				},
			},
			ironman_manager: {
				date: "1066.9.15",
				save_game: "",
				save_storage: "local",
			},
			date: "1398.3.1",
			bookmark_date: "1066.9.15",
		}
	);
});

describe("parseFile()", () => {
	it("parses a save file", async () => {
		expect.assertions(1);

		const file = await fs.readFile(path.join("test", "sample.ck3"), "binary");
		const result = await parseFile(file);

		expect(result).toEqual({
			meta_data: {
				save_game_version: "3",
				version: "1.2.1",
				meta_coat_of_arms: {
					pattern: "pattern_solid.dds",
					color1: "red",
					color2: "yellow",
				},
			},
			ironman_manager: {
				date: "1066.9.15",
				save_game: "",
				save_storage: "local",
			},
			date: "1398.3.1",
			bookmark_date: "1066.9.15",
		});
	});
});
