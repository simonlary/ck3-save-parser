import { parse } from "../src/parser";

// eslint-disable-next-line @typescript-eslint/no-explicit-any
function parserTest(name: string, input: string[], output: any) {
	it(name, () => {
		expect(parse(input)).toEqual(output);
	});
}

describe("Parser", () => {
	parserTest("parses an empty array.", [], {});

	parserTest("parses a simple key/value pair.", ["key", "=", "value"], {
		key: "value",
	});

	parserTest("parses a single object with single key/value pair.", ["object", "=", "{", "key", "=", "value", "}"], {
		object: {
			key: "value",
		},
	});

	parserTest(
		"parses a single object with multiple key/value pair.",
		["object", "=", "{", "key1", "=", "value1", "key2", "=", "value2", "}"],
		{
			object: {
				key1: "value1",
				key2: "value2",
			},
		}
	);

	it("throws when it gets an invalid object. (missing '=')", () => {
		expect(() => parse(["key1", "=", "value1", "key2", "value2"])).toThrow("missing '='");
	});

	parserTest("parses a value array.", ["array", "=", "{", "value1", "value2", "}"], {
		array: ["value1", "value2"],
	});

	parserTest("parses a value array with a missing ending bracket.", ["array", "=", "{", "value1", "value2"], {
		array: ["value1", "value2"],
	});

	parserTest(
		"parses an object array.",
		["array", "=", "{", "{", "key1", "=", "value1", "}", "{", "key2", "=", "value2", "}", "}"],
		{
			array: [
				{
					key1: "value1",
				},
				{
					key2: "value2",
				},
			],
		}
	);

	parserTest(
		"parses a 'repeated' array.",
		["object", "=", "{", "array", "=", "value1", "array", "=", "value2", "array", "=", "value3", "}"],
		{
			object: {
				array: ["value1", "value2", "value3"],
			},
		}
	);

	parserTest("parses an rgb object.", ["color", "=", "rgb", "{", "3", "14", "16", "}"], {
		color: ["3", "14", "16"],
	});
});
